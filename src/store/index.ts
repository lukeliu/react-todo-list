import { StoreState } from '../types';
import { ADD_TODO, AddTodoAction, AddTodoReducer } from './AddTodo';
import { MODIFY_CATEGORY_FILTER, ModifyCategoryFilterAction, ModifyCategoryFilterReducer } from './ModifyCategoryFilter';
import { MODIFY_TODO, ModifyTodoAction, ModifyTodoReducer } from './ModifyTodo';
import { REMOVE_TODO, RemoveTodoAction, RemoveTodoReducer } from './RemoveTodo';
import { REORDER_TODO, ReorderTodoAction, ReorderTodoReducer } from './ReorderTodo';

export * from './AddTodo';
export * from './ModifyCategoryFilter';
export * from './ModifyTodo';
export * from './RemoveTodo';
export * from './ReorderTodo';

const initialState: StoreState = {
  categoryFilter: null,
  todos: []
}

type Action = ModifyCategoryFilterAction | AddTodoAction | RemoveTodoAction | ModifyTodoAction | ReorderTodoAction

export function reducer(state: StoreState = initialState, action: Action): StoreState {
  switch (action.type) {
    case ADD_TODO: {
      return AddTodoReducer(state, action);
    }
    case MODIFY_CATEGORY_FILTER: {
      return ModifyCategoryFilterReducer(state, action);
    }
    case MODIFY_TODO: {
      return ModifyTodoReducer(state, action);
    }
    case REMOVE_TODO: {
      return RemoveTodoReducer(state, action);
    }
    case REORDER_TODO: {
      return ReorderTodoReducer(state, action);
    }
  }

  return state;
}