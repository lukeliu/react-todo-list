import { StoreState, TodoStatus } from '../types';

export const MODIFY_TODO = 'MODIFY_TODO';

interface ModifyTodoPayload {
  id: number;
  title: string;
  status: TodoStatus;
}

export interface ModifyTodoAction extends ModifyTodoPayload {
  type: typeof MODIFY_TODO;
}

export function modifyTodo(id: number, title: string, status: TodoStatus): ModifyTodoAction {
  return {
    type: MODIFY_TODO,
    id,
    title,
    status
  }
}

export function ModifyTodoReducer(state: StoreState, payload: ModifyTodoPayload): StoreState {
  const indexToModify = state.todos.findIndex(todo => todo.id === payload.id)
  const newTodos = [ ...state.todos ];
  newTodos[indexToModify] = { 
    ...state.todos[indexToModify],
    title: payload.title,
    status: payload.status
  };
  return {
    ...state,
    todos: newTodos
  }
}