import { StoreState } from '../types';

export const REORDER_TODO = 'REORDER_TODO';

interface ReorderTodoPayload {
  id: number;
  targetId: number;
}

export interface ReorderTodoAction extends ReorderTodoPayload {
  type: typeof REORDER_TODO;
}

export function reorderTodo(id: number, targetId: number): ReorderTodoAction {
  return {
    type: REORDER_TODO,
    id,
    targetId
  }
}

export function ReorderTodoReducer(state: StoreState, payload: ReorderTodoPayload): StoreState {
  const newTodos = [ ...state.todos ];
  const todoToBeReorderedIndex = newTodos.findIndex(todo => todo.id === payload.id);
  const todoToBeReordered = newTodos.splice(todoToBeReorderedIndex, 1)[0];
  const indexToInsert = newTodos.findIndex(todo => todo.id === payload.targetId);
  newTodos.splice(indexToInsert >= todoToBeReorderedIndex ? (indexToInsert + 1) : (indexToInsert), 0, todoToBeReordered);
  return {
    ...state,
    todos: newTodos
  }
}