import { StoreState, TodoStatus } from '../types';

export const ADD_TODO = 'ADD_TODO';

interface AddTodoPayload {
  todo: {
    id: number;
    title: string;
    status: TodoStatus;
  }
}

export interface AddTodoAction extends AddTodoPayload {
  type: typeof ADD_TODO;
}

export function addTodo(title: string, status: TodoStatus, id: number = new Date().getTime()): AddTodoAction {
  return {
    type: ADD_TODO,
    todo: {
      id,
      title,
      status
    }
  }
}

export function AddTodoReducer(state: StoreState, payload: AddTodoPayload): StoreState {
  return {
    ...state,
    todos: [
      ...state.todos,
      payload.todo
    ]
  }
}