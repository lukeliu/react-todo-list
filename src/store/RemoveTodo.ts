import { StoreState } from '../types';

export const REMOVE_TODO = 'REMOVE_TODO';

interface RemoveTodoPayload {
  id: number;
}

export interface RemoveTodoAction extends RemoveTodoPayload {
  type: typeof REMOVE_TODO;
}

export function removeTodo(id: number): RemoveTodoAction {
  return {
    type: REMOVE_TODO,
    id
  }
}

export function RemoveTodoReducer(state: StoreState, payload: RemoveTodoPayload): StoreState {
  return {
    ...state,
    todos: state.todos.filter(todo => todo.id !== payload.id)
  }
}
