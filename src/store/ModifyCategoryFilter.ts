import { StoreState, TodoStatus } from '../types';

export const MODIFY_CATEGORY_FILTER = 'MODIFY_CATEGORY_FILTER';

interface ModifyCategoryFilterPayload {
  category: TodoStatus | null;
}

export interface ModifyCategoryFilterAction extends ModifyCategoryFilterPayload {
  type: typeof MODIFY_CATEGORY_FILTER;
}

export function modifyCategoryFilter(category: TodoStatus | null): ModifyCategoryFilterAction {
  return {
    type: MODIFY_CATEGORY_FILTER,
    category
  }
}

export function ModifyCategoryFilterReducer(state: StoreState, payload: ModifyCategoryFilterPayload): StoreState {
  return {
    ...state,
    categoryFilter: payload.category
  }
}