import './global.scss';
import React from 'react';
import ReactDOM from 'react-dom';
import { createStore } from 'redux';
import { Provider } from 'react-redux';
import { App } from './components/App/App';
import reportWebVitals from './reportWebVitals';
import { reducer, addTodo } from './store';
import { loadState, saveState } from './localStorage';
import { getTodos } from './api';

const persistedState = loadState();
const store = createStore(reducer, persistedState);

if (persistedState === undefined) {
  getTodos().then(todos => {
    for (let i = 0; i < 10 && i < todos.length; i++) {
      const todo = todos[i];
      store.dispatch(addTodo(todo.title, todo.status, todo.id));
    }
  });
}

store.subscribe(() => {
  saveState(store.getState());
});

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <App />
    </Provider>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
