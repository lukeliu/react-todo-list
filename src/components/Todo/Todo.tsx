import { ChangeEvent } from 'react';
import { useDispatch } from 'react-redux';
import { faCheck, faTimes, faUndo } from "@fortawesome/free-solid-svg-icons";
import './Todo.scss';
import { TodoStatus } from '../../types';
import { removeTodo, modifyTodo } from '../../store';
import { ClickableIcon } from '../ClickableIcon/ClickableIcon';

export interface Props {
  id: number;
  title: string;
  status: TodoStatus;
}

export function Todo({ id, title, status }: Props) {
  const dispatch = useDispatch();

  const onChangeTitle = (event: ChangeEvent<HTMLTextAreaElement>) => {
    dispatch(modifyTodo(id, event.target.value, status));
  }

  const onUpdateStatus = () => {
    dispatch(modifyTodo(id, title, status === TodoStatus.ACTIVE ? TodoStatus.COMPLETED : TodoStatus.ACTIVE));
  }

  const onRemoveTodo = () => {
    dispatch(removeTodo(id));
  }

  return (
    <div className={'todo ' + (status === TodoStatus.ACTIVE ? 'active' : 'completed')}>
      <div className="header">
        <div className="button-group">
          <ClickableIcon icon={status === TodoStatus.ACTIVE ? faCheck : faUndo} onClick={onUpdateStatus} />
          <ClickableIcon icon={faTimes} onClick={onRemoveTodo} />
        </div>
      </div>

      <div className="body">
        <textarea value={title} onChange={onChangeTitle}></textarea>
      </div>
    </div>
  )
}