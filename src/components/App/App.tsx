import { useSelector } from 'react-redux';
import './App.scss';
import { TodoList } from '../TodoList/TodoList';
import { StoreState } from '../../types';

export function App() {
  const todos = useSelector((state: StoreState) => state.todos);

  return (
    <div className="app">
      <div className="list-wrapper">
        <TodoList todos={todos} />
      </div>
    </div>
  );
}
  