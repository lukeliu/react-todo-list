import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IconDefinition } from "@fortawesome/free-solid-svg-icons";
import './ClickableIcon.scss';

export interface Props {
  icon: IconDefinition,
  onClick?: () => void
}

export function ClickableIcon({ icon, onClick = () => {} }: Props) {
  return (
    <FontAwesomeIcon className="clickable-icon" icon={icon} onClick={onClick} />
  )
}