import { ChangeEvent, useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import './TodoList.scss'
import { Todo } from '../Todo/Todo';
import { Todo as TodoModel, TodoStatus, StoreState } from '../../types';
import { modifyCategoryFilter, addTodo, reorderTodo } from '../../store';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlus } from "@fortawesome/free-solid-svg-icons";

export interface Props {
  todos: TodoModel[];
}

function mapStatusToCategory(status: TodoStatus | null): string {
  switch (status) {
    case TodoStatus.ACTIVE: 
      return 'active';
    case TodoStatus.COMPLETED: 
      return 'completed';
    default: 
      return 'all';
  }
}

function mapCategoryToStatus(category: string): TodoStatus | null {
  switch (category) {
    case 'active': 
      return TodoStatus.ACTIVE;
    case 'completed': 
      return TodoStatus.COMPLETED;
    default: 
      return null;
  }
}

export function TodoList({ todos }: Props) { 
  let bottomReference: HTMLDivElement | null;

  const [ todoBeingDragged, setTodoBeingDragged ] = useState<number | undefined>(undefined);
  const [ scroll, setScroll ] = useState<boolean>(false);

  useEffect(() => {
    if (scroll) {
      bottomReference?.scrollIntoView({ behavior: 'smooth' });
      setScroll(false);
    }
  }, [scroll]);
  
  const dispatch = useDispatch();
  const categoryFilter = useSelector((state: StoreState) => state.categoryFilter);
  const selectValue = mapStatusToCategory(categoryFilter);

  const onChangeCategoryFilter = (event: ChangeEvent<HTMLSelectElement>) => {
    dispatch(modifyCategoryFilter(mapCategoryToStatus(event.target.value)));
  }

  const visibleTodos = categoryFilter != null ? todos.filter(todo => todo.status === categoryFilter) : todos;

  const onDragStart = (id: number) => {
    setTodoBeingDragged(id);
  }

  const onDragOver = (id: number) => {
    if (id && todoBeingDragged && id !== todoBeingDragged) {
      dispatch(reorderTodo(todoBeingDragged, id));
    }
  }

  const onDragEnd = () => {
    setTodoBeingDragged(undefined);
  }

  const onAddTodo = () => {
    dispatch(addTodo('(click to edit, drag the item to reorder)', TodoStatus.ACTIVE));
    if (categoryFilter === TodoStatus.COMPLETED) {
      dispatch(modifyCategoryFilter(null));
    }
    setScroll(true);
  }

  return (
    <div className="todo-list">
      <div className="list-header">
        <select value={selectValue} onChange={onChangeCategoryFilter}>
          <option value="all">All</option>
          <option value="active">Active</option>
          <option value="completed">Completed</option>
        </select>
      </div>
      <div className="list-body">
        {
          visibleTodos.map(({ id, title, status }) => (
            <div key={id} 
              className={'drag-wrapper ' + (id === todoBeingDragged ? 'dragging' : '')}
              draggable={true}
              onDragStart={() => onDragStart(id)}
              onDragOver={(e) => onDragOver(id)}
              onDragEnd={onDragEnd}>
              <Todo id={id} title={title} status={status} />
            </div>
          ))
        }
        <div ref={e => bottomReference = e}></div>
      </div>
      <div className="add-todo" onClick={onAddTodo}>
        <span>
          <FontAwesomeIcon icon={faPlus} />
          Add Todo</span>
      </div>
    </div>
  )
}
