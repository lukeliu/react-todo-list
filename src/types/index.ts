export enum TodoStatus {
  'ACTIVE' = 'ACTIVE',
  'COMPLETED' = 'COMPLETED'
}

export interface Todo {
  id: number;
  title: string;
  status: TodoStatus;
}

export interface StoreState {
  categoryFilter: TodoStatus | null;
  todos: Todo[];
}