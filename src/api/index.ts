import { Todo, TodoStatus } from '../types';

export async function getTodos(): Promise<Todo[]> {
  interface ResponseObject {
    completed: boolean;
    id: number;
    title: string;
    userId: number;
  }

  const response: Response = await fetch('https://jsonplaceholder.typicode.com/todos');
  const responseObject: ResponseObject[] = await response.json();

  return responseObject.map(obj => ({
    id: obj.id,
    title: obj.title,
    status: obj.completed ? TodoStatus.COMPLETED : TodoStatus.ACTIVE
  }));
}