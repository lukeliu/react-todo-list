# React Todo List

This project is implemented with React. 

## Features

- Click "Add Todo" to add new todo item
- Click an existing todo to modify the content
- Click the cross icon to remove todos
- Use the select box to filter the todo list
- Drag and drop to reorder todos
- The todo list will be persisted automatically via localStorage
- The first 10 records from https://jsonplaceholder.typicode.com/todos (regardless of the userId) will be fetched at first load

## How to Run
1. Run `npm install` to install the dependencies
2. Run `npm run start` to start the application locally
3. Visit http://localhost:3000/

## Others
- Around 5 hours were used to develop this application
- This application is developed and tested with only Google Chrome, the support of other browsers is not guaranteed
